FROM gitlab/gitlab-runner

LABEL url="https://gitlab.com/ringingmountain/docker/podman-gitlab-runner" authors='robin@ringingmountain.com'

ENV STORAGE_DRIVER=vfs

RUN apt-get update -qq \
 && apt-get install -qq -y software-properties-common uidmap curl \
 && add-apt-repository -y ppa:projectatomic/ppa \
 && apt-get update -qq \
 && apt-get -qq -y install git iptables podman python3 python3-pip \
 && ln -s /usr/bin/podman /usr/bin/docker

WORKDIR /docs
