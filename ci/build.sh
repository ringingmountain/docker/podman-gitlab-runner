#!/usr/bin/env sh
set -e

. ci/common.sh

TAG_ARGS="--tag ${LATEST} --tag ${TAG}"
test -n "$1" && TAG_ARGS="${TAG_ARGS} --tag ${CI_REGISTRY_IMAGE}:${1}"

log_run 'Building Docker image' docker build --pull --no-cache $TAG_ARGS .

if test -n "$1"
then
  docker tag "${CI_REGISTRY_IMAGE}:${1}"
fi
