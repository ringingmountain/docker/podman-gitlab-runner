project = 'Podman Gitlab Runner'
copyright = '2019 Ringing Mountain, LLC'
author = 'Robin Klingsberg'

version = '0.0.0'

source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

htmlhelp_basename = "podman_gitlab_ci_runner_doc"
